# How to install phoenix os on linux system

  1. At first create phoenix folder inside root directory.
   ```
   # mkdir -p /phoenix/data
   ```
  2. Extract `PhoenixOSInstaller_v3.6.1.564_x64.iso` file.
  ```
  # 7z x -y PhoenixOSInstaller_v3.6.1.564_x64.iso
  ```
  3. After extracting, delete `PhoenixOSInstaller_v3.6.1.564_x64.iso` file from current directory for safefy. 
  4. Then extract `system.sfs` file. 
  ```
  # unsquashfs system.sfs
  ```
  > After compleating this process, please delete `system.sfs` file.
  5. Open `squashfs-root` directory and copy `system.img` to `/phoenix`. And then delete `squashfs-root` folder.
  ```
  # cp -r system.img /phoenix
  ```
  6. Copy all remaining files into `/phoenix` folder. The selection file is:
  ``` 
  '[BOOT]'   initrd.img    isolinux   ramdisk.img   TRANS.TBL
   efi       install.img   kernel     system.img


  Example :
  # cp -rv * /phoenix/
  ```
  7. Install `grub-customizer` for updating **grub** or you can do it manually by terminal. 
  ```
  ubuntu 
  # apt install grub-customizer

  arch 
  # pacman -S grub-customizer
  ```
  8. Open `grub-customizer` then create a new entry. And **name** it to **"Phoenix OS"** then select type as `other`
  9. Before going next step check your **root** partition location. We can check it using `lablk`. 
  Output looks like this : 
  ```
  NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
  sda      8:0    0   20G  0 disk 
  ├─sda1   8:1    0    1M  0 part 
  ├─sda2   8:2    0  513M  0 part /boot/efi
  └─sda3   8:3    0 19.5G  0 part /

  ```
  > **NOTE** <br>
  > In my case my system **root** is `/dev/sda3`. Which is mean the value is `3`. <br>

  10. Just copy and paste those into **Boot Squence** section. 
  > Note : enter your hdd value in 2nd line, look like `set root='hd0,<your root value>'`
  ```
  insmod part_gpt 
  set root='hd0,3' 
  linux /phoenix/kernel root=/dev/ram0 androidboot.hardware=android_x86 androidboot.selinux=permissive quiet SRC=/phoenix/ DATA=/data/ vga=788
  initrd /phoenix/initrd.img

  ```
  11. Save it. Make sure you've updated your grub. And then reboot your pc.
  ```
  # sudo update-grub
  ```
  12. I hope everything works fine. Thank you. 
  
## Written By,
Farhan Sadik
